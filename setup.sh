#!/bin/sh

. ./utils.sh
CURDIR="$(pwd)"
TMP="$(mktemp -d)"

fallback_ls() {
    sed -i 's/exa -laF --group-directories-first --header --git --long/ls  -laF --group-directories-first/g' ~/.myenv
    sed -i 's/exa -a --group-directories-first/ls -a --group-directories-first/g' ~/.myenv
    sed -i 's/exa -lF --group-directories-first --header --git --long/ls -lF --group-directories-first/g' ~/.myenv
    sed -i 's/exa --group-directories-first/ls --group-directories-first/g' ~/.myenv
}

cd "$TMP" || exit


apt_install(){
    pkg_list="git curl wget zip unzip sed build-essential make shellcheck doxygen zsh tmux terminator vim nano tldr exa htop tree jq apt-transport-https daemontools"

    echo_info "Updating APT repositories"
    sudo apt update --quiet
    sudo apt upgrade -y --quiet

    for pkg in $pkg_list:
    do
        echo_info "Installing ${pkg}"
        if sudo apt-get -yqq --quiet install "$pkg"; then
        echo_ok "$pkg installed."
        else
        echo_err "$pkg not installed!"
        fi
    done
}

snap_install() {
    echo_info "Installing Snap packages"
    sudo snap install chromium drawio vlc
    sudo snap install intellij-idea-community --classic
    sudo snap install teams keepassxc
    sudo snap install code --classic
}

vscode_config(){
    echo_info "Installing VS Code extensions"
    extensions_list="octref.vetur ms-python.python ms-python.vscode-pylance esbenp.prettier-vscode yzhang.markdown-all-in-one eamodio.gitlens hediet.vscode-drawio redhat.vscode-yaml VisualStudioExptTeam.vscodeintellicode vscjava.vscode-java-pack vscjava.vscode-spring-boot-dashboard wolfmah.ansible-vault-inline formulahendry.auto-close-tag formulahendry.auto-rename-tag ms-vscode.cpptools twxs.cmake golang.go marp-team.marp-vscode jebbs.plantuml rust-lang.rust-analyzer"

    for extension in $extensions_list:
    do
        echo_info "Installing ${extension}"
        if code -r --install-extension "$extension" --force; then
        echo_ok "$pkg installed."
        else
        echo_err "$pkg not installed!"
        fi
    done

} 

java_install(){
    echo_info "Installing Java 17"
    wget -O - https://packages.adoptium.net/artifactory/api/gpg/key/public | sudo tee /usr/share/keyrings/adoptium.asc
    echo "deb [signed-by=/usr/share/keyrings/adoptium.asc] https://packages.adoptium.net/artifactory/deb focal main" | sudo tee /etc/apt/sources.list.d/adoptium.list
    sudo apt-get update
    sudo apt-get install temurin-17-jdk
}

nvm_install(){
    echo_info "Installing NVM"
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
    nvm install --lts
    npm install --global yarn
}

docker_install(){
    sudo apt-get remove docker docker-engine docker.io containerd runc
    sudo apt-get update
    sudo apt-get install ca-certificates curl gnupg lsb-release
    sudo mkdir -p /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt-get update
    sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
    sudo groupadd docker
    sudo usermod -aG docker $USER
    newgrp docker 
}

pip_install(){
    echo_info "Installing pip"
    sudo apt install python3-pip
    echo_info "Installing pip succeed"

    echo_info "Installing pipenv"
    pip install pipenv
    echo_info "Installing pipenv succeed"
}

exa_install(){
    if ! which exa; then
        echo_info "Trying to install exa manually since it was not installed automatically"
        if wget "https://github.com/ogham/exa/releases/download/v0.10.1/exa-linux-x86_64-v0.10.1.zip"; then
            unzip exa-linux-x86_64-v0.10.1.zip
            if sudo mv bin/exa /usr/bin/exa; then
            echo_ok "Exa installed."
            else
            echo_err "Could not install exa, falling back to ls"
            fallback_ls
            fi
        else
            echo_err "Could not install exa, falling back to ls"
            fallback_ls
        fi
    fi
}

rust_install(){
    echo_info "Installing Rust"
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
}

git_config(){
    echo_info "Configuring git"
    git config --global user.name "Charles CERISIER"
    git config --global user.email "charlescerisier@gmail.com"
}


zsh_install() {
    echo_info "Installing ohmyzsh"
    if sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" --keep-zshrc --unattended; then
        echo_ok "OhMyZsh installed."
        echo_info "Copying zsh config"
        cp "${CURDIR}/res/zshrc" ~/.zshrc

        if cp "${CURDIR}/res/af-magic-totok.zsh-theme" ~/.oh-my-zsh/themes/af-magic-totok.zsh-theme; then
            echo_info "Using custom zsh theme"
        else
            echo_err "Could not cp custom zsh theme, falling back to default theme"
            sed -i 's/af-magic-totok.zsh-theme/af-magic.zsh-theme/g' ~/.zshrc
        fi

        echo_info "Setting up zsh as default prompt"
        if zsh=$(command -v zsh); then
            if sudo chsh -s "$zsh" "$USER"; then
                echo_ok "Shell setup OK"
            else
                echo_err "Could not set default shell!"
            fi
        else
            echo_err "Could not set default shell!"
        fi
    else
        echo_err "Error while installing OhMyZsh"
    fi
}

tmux_config() {
    echo_info "Setting up tmux"
    if git clone https://github.com/jimeh/tmux-themepack.git ~/.tmux-themepack; then
        echo_info "Copying zsh config"
        if cp "${CURDIR}/res/tmux.conf" ~/.tmux.conf; then
            echo_ok "Tmux configured"
        else
            echo_err "Tmux config has not been copied !"
        fi
    else
        echo_err "Tmux themes could not be installed, skipping tmux config!"
    fi

    echo_info "Installing Tmux Plugin Manager"
    mkdir -p ~/.tmux/plugins/
    if git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm; then
        echo_ok "TPM installed"
    else
        echo_err "TPM could not be installed!"
    fi

}

terminator_config() {
    echo_info "Setting up terminator config"
    mkdir -p ~/.config/terminator
    if cp "${CURDIR}/res/terminator_config" ~/.config/terminator/config; then
        echo_ok "Terminator configured"
    else
        echo_err "Terminator config has not been copied!"
    fi
}

firacodefont_install(){
    echo_info "Installing FiraCode font"
    mkdir -p firacodefont
    cd firacodefont
    if wget "https://github.com/tonsky/FiraCode/releases/download/6.2/Fira_Code_v6.2.zip"; then
        unzip "Fira_Code_v6.2.zip"
        mkdir ~/.fonts
        cp ttf/* ~/.fonts
        fc-cache -f
    else
        echo_err "Could not install FiraCode!"
    fi
    cd ..
}

vim_config() {
    echo_info "Setting up Vim config"
    if cp "${CURDIR}/res/vimrc" ~/.vimrc; then
        echo_ok "Vim configured"
    else
        echo_err "Vim config has not been copied!"
    fi
    cd - || exit
    rm -rf "$TMP"
}

all() {
    apt_install
    snap_install
    docker_install
    rust_install
    pip_install
    nvm_install
    java_install
    zsh_install
    exa_install
    git_config
    vscode_config
    firacodefont_install
    terminator_config
    tmux_config
    vim_config

}

echo_info "Setting up my environment"

while getopts 'hi:f:' OPTION; do
    case "$OPTION" in
        h)
            echo "Usage: $0 [-h] args [* || apt || snap || docker || pip || nvm || java || zsh || exa || git || vscode || terminator || firacodefont || tmux || vim]  " >&2
            exit 1
        ;;
    esac
done

shift "$(($OPTIND -1))"


for arg in "$@"; do
    if [ "$arg" = "*" ]; then
        all
        return
    elif [ "$arg" = "apt" ]; then
        apt_install
    elif [ "$arg" = "snap" ]; then
        snap_install
    elif [ "$arg" = "docker" ]; then
        docker_install
    elif [ "$arg" = "pip" ]; then
        pip_install
    elif [ "$arg" = "nvm" ]; then
        nvm_install
    elif [ "$arg" = "java" ]; then
        java_install
    elif [ "$arg" = "zsh" ]; then
        zsh_install
    elif [ "$arg" = "exa" ]; then
        exa_install
    elif [ "$arg" = "git" ]; then
        git_config
     elif [ "$arg" = "vscode" ]; then
        vscode_config
    elif [ "$arg" = "terminator" ]; then
        terminator_config
    elif [ "$arg" = "firacodefont" ]; then
        firacodefont_install
    elif [ "$arg" = "tmux" ]; then
        tmux_config
    elif [ "$arg" = "vim" ]; then
        vim_config
    else
        echo_err "${arg} command not found"
    fi
done
